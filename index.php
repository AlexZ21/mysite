<?php

require_once 'application/Core/Application.php';
try {
    $app = new \Core\Application();
    $app->run();
} catch (\Core\Ex $e) {
    $e->printError();
}
?>