$(document).ready(function(){
    
    $("input[name=button-save]").click(function(){
       $("form[name=form-item-edit]").submit(); 
    });
    
    $("input[name=button-remove]").click(function(){
        $("form[name=form-item-edit]").attr('action', '/directories/remove');
       $("form[name=form-item-edit]").submit(); 
    });
    
})
