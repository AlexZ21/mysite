$(document).ready(function(){
    
    var nextAttr = 1;
    
    $("input[name=button-add-attr]").click(function(){
        
      $("#tmp").find(".attr-id").attr('name', 'attrid'+nextAttr); 
      $("#tmp").find(".attr-name").attr('name', 'attrname'+nextAttr); 
      $("#tmp").find(".attr-type").attr('name', 'attrtype'+nextAttr); 
      
      
      var dirAttrTemplate = $("#tmp").html();
      $(".directory-attrs").append(dirAttrTemplate);
      nextAttr++;
      
      $(".dir-attr-template").on('click', ".button-remove-attr", function(){
        $(this).parent(".dir-attr-template").remove();
      });
      
    });
    
    $("input[name=button-save]").click(function(){
        $("form[name=form-directory-add]").submit(); 
    });

});
