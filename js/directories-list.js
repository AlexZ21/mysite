$(document).ready(function(){
    
    $("input[name=button-add]").click(function(){
       $("form[name=form-directory-list]").submit(); 
    });
    
    $("input[name=button-remove]").click(function(){
        $("form[name=form-directory-list]").attr('action', '/directories/remove');
       $("form[name=form-directory-list]").submit(); 
    });
    
})
