$(document).ready(function() {
    $("input[name=button-remove]").click(function() {
        $("form[name=form-item-list]").attr('action', '/directories/remove');
        $("form[name=form-item-list]").submit();
    });
    $("input[name=button-removeall]").click(function() {
        $("form[name=form-item-list]").attr('action', '/directories/removeall');
        $("form[name=form-item-list]").submit();
    });    
    $("input[name=button-selectall]").click(function() {
        var checkBoxes = $(".directory-item-list-row-checkbox");
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });
        $("input[name=button-edit]").click(function() {
        $("form[name=form-item-list]").attr('action', '/directories/edit');
        $("form[name=form-item-list]").submit();
    }); 
})
