<?php 
/** Fenom template 'auth.html' compiled at 2014-08-04 01:54:12 */
return new Fenom\Render($fenom, function ($var, $tpl) {
?><!DOCTYPE html>
<html>
    <head>
        <title>Авторизация</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <div class="auth-block">
            <form method="post" action="/Auth">
                <div>Авторизация</div>
                <div>
                    <input type="text" name="login" placeholder="Логин">
                    <input type="text" name="password" placeholder="Пароль">
                </div>
                <div>
                    <input type="submit" value="Вход">
                </div>
            </form>
        </div>
    </body>
</html>
<?php
}, array(
	'options' => 1024,
	'provider' => false,
	'name' => 'auth.html',
	'base_name' => 'auth.html',
	'time' => 1407102835,
	'depends' => array (
  0 => 
  array (
    'auth.html' => 1407102835,
  ),
),
	'macros' => array(),

        ));
