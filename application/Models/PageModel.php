<?php

namespace Models;

/**
 * 
 */
class PageModel extends \Models\BaseModel {

    private $page;
    private $pageTitle;
    private $pageSettings;
    private $scripts = [];

    public function __construct($app) {
        parent::__construct($app);
        $settingsPath = $this->app->getSettings()->get('appDir') . "/settings.json";

        if (file_exists($settingsPath)) {
            $jsonString = file_get_contents($settingsPath);
            $this->pageSettings = json_decode($jsonString, TRUE);
        }
    }

    public function setPage(&$page) {
        $this->page = $page;
    }

    public function setMenuTitle($title) {
        $this->pageTitle = $title;

    }

    public function makeHeader() {
        $headerPart = &$this->page->getPagePart('header');
        $headerPart->set('title', $this->pageTitle);
        $headerPart->set('username', $this->getHeaderUsername());

    }

    public function getHeaderUsername() {
        $userManager = &$this->app->getModule('UserManager');

        $user = $userManager->getCurrentUser();
        
        if ($user) {
            return $user->getAttr('Name') . " " . $user->getAttr('Surname');
        } else {
            return "";
        }
    }
    
    
    public function addScript($name) {
        $this->scripts[] = $name;
    }
    
    public function makeMeta() {
        $headerPart = &$this->page->getPagePart('meta');
        $headerPart->set('scripts', $this->scripts);
    }

    public function makePage() {
        $this->page->setTitle($this->pageTitle);
        $this->makeMeta();
        $this->makeHeader();
    }

}
