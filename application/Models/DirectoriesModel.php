<?php

namespace Models;

/**
 * Description of Directories
 *
 * @author alex
 */
class DirectoriesModel extends \Models\BaseModel {

    public function __construct($app) {
        parent::__construct($app);
    }

    public function getDirectories() {
        $table = $this->app->getModule('DBManager')->getTable('Directories');
        $result = $table->select(['columns' => ['Title', 'ID', 'SP'],
            'orderby' => ['Title ASC']]
        );

        $directories = [];

        while ($result->next()) {
            $directories[] = ['title' => $result->get('Title'),
                'url' => '/directories/show?sp=' . $result->get('SP'),
                'sp' => $result->get('SP')];
        }

        return $directories;
    }

    public function getDirectoryInfo($sp) {
        $table = $this->app->getModule('DBManager')->getTable('Directories');
        $result = $table->select(['columns' => ['Title', 'ID'],
            'where' => ['SP = ?i', $sp], 'limit' => 1]);
        $result->next();
        return ['Title' => $result->get('Title'), 'ID' => $result->get('ID')];
    }

    public function updateDirectoryItem($dirSP, $spItemId) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');

        if ($dbObject) {
            $query = [];
            $attrs = $dbObject->getAttrs();

            $comma = "";
            $columns = "";
            $set = [];
            foreach ($attrs as $attr) {
                if ($attr['show']) {
                    $columns .= $comma . $attr['name'] . " = ?" . $attr['type'];
                    $set[] = $this->app->getModule('Request')->getParameter($attr['name']);
                    $comma = ", ";
                }
            }

            array_unshift($set, $columns);
            $dbObject->update(['set' => $set, 'where' => ['ID = ?i', $spItemId]]);
        }
    }

    public function addDirectoryItem($dirSP, $spItemId) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');
        if ($dbObject) {

            $attrs = $dbObject->getAttrs();
            foreach ($attrs as $attr) {
                if ($attr['show']) {
                    $values[$attr['name']] = $this->app->getModule('Request')->getParameter($attr['name']);
                }
            }

            $dbObject->insert($values);
        }
    }

    public function removeDirectoryItem($dirSP, $spItemId) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');
        if ($dbObject) {
            $dbObject->removeById($spItemId);
        }
    }

    public function removeDirectoryItems($dirSP, $spItemsId = []) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');
        if ($dbObject) {
            $parameters = $this->app->getModule('Request')->getParameters();
            foreach ($spItemsId as $value) {
                $dbObject->removeById($value);
            }
        }
    }

    public function removeAllItems($dirSP) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');
        if ($dbObject) {
            $dbObject->removeAll();
        }
    }

    public function addDirectory($dirName, $dirSP, $dirAttrs) {
        $dbManager = &$this->app->getModule('DBManager');
        $dboManager = &$this->app->getModule('DBObjectManager');
        $dirTable = $this->app->getModule('DBManager')->getTable('Directories');
        $result = $dirTable->select(['columns' => ['Title', 'ID'],
            'where' => ['SP = ?i', $dirSP], 'limit' => 1]);

        if (!$result->rowCount()) {
            $dirTable->insert(['columns' => 'SP, Title', 'values' => ['?i, ?s', $dirSP, $dirName]]);

            $sql = "CREATE TABLE IF NOT EXISTS `SP" . $dirSP . "` (
                    `ID` int(11) NOT NULL AUTO_INCREMENT, ";

            foreach ($dirAttrs as $key => $value) {
                $type = $dboManager->getType($value['attrtype']);
                switch ($type[1]) {
                    case 'varchar':
                        $sql .= "`Attr" . $key . "` " . $type[1] . "(255)  CHARACTER SET utf8 NOT NULL,";
                        break;
                    case 'int':
                        $sql .= "`Attr" . $key . "` " . $type[1] . "(255)  NOT NULL,";
                    default:
                        break;
                }
            }

            $sql .= " PRIMARY KEY (`ID`)
                  ) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=0;";

            $stmt = $dbManager->createStatement();
            $stmt->executeQuery($sql);

            $objTable = $this->app->getModule('DBManager')->getTable('Objects');
            $objTable->insert(['columns' => 'Name, DbTable', 'values' => ['?s, ?s', "SP" . $dirSP . "Object", "SP" . $dirSP]]);

            $result = $objTable->select(['columns' => ['ID', 'Name'],
                'where' => ["Name LIKE ?s", "SP" . $dirSP . "Object"], 'limit' => 1]);

            if ($result->rowCount()) {
                $result->next();
                $objId = $result->get('ID');

                $objAttrTable = $this->app->getModule('DBManager')->getTable('ObjectAttributes');

                $objAttrTable->insert(['columns' => '`ID`, `ObjectId`, `Name`, `Description`, `HtmlType`,'
                    . ' `DbColumn`, `Show`',
                    'values' => ["NULL, ?s, 'ID', '', 'number', '', '0'", $objId]]);

                foreach ($dirAttrs as $key => $value) {
                    $objAttrTable->insert(['columns' => '`ID`, `ObjectId`, `Name`, `Description`, `HtmlType`,'
                        . ' `DbColumn`, `Show`',
                        'values' => ["NULL, ?s, ?s, ?s, ?s, '', '1'",
                            $objId, 'Attr' . $key, $value['attrname'], $value['attrtype']]]);
                }
            }
        }
    }

    public function removeDirectory($dirsSP) {
        foreach ($dirsSP as $dirSP) {
            $this->removeDirectoryBySP($dirSP);
        }
    }

    public function removeDirectoryBySP($dirSP) {
        $dbManager = &$this->app->getModule('DBManager');


        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');
        if ($dbObject) {
            $stmt = $dbManager->createStatement();
            $stmt->executeQuery("DROP TABLE SP" . $dirSP);
            
            $dirTable = $this->app->getModule('DBManager')->getTable('Directories'); 
            $dirTable->remove(['where' => ['SP = ?i', $dirSP]]);
            
            $objTable = $this->app->getModule('DBManager')->getTable('Objects'); 
            $result = $objTable->select(['columns' => ['ID'],
                'where' => ["Name LIKE ?s", "SP" . $dirSP . "Object"], 'limit' => 1]);
            $result->next();
            $objId = $result->get('ID');
            $objTable->remove(['where' => ['ID = ?i', $objId]]);
            
            $objAttrTable = $this->app->getModule('DBManager')->getTable('ObjectAttributes');
            $objAttrTable->remove(['where' => ['ObjectID = ?i', $objId]]);
            
        }
    }

}
