<?php

namespace Models;
/**
 * Description of BaseModel
 *
 * @author alex
 */
class BaseModel extends \Core\Component {
    
    public function __construct(&$app) {
        parent::__construct($app);
    }
    
}
