<?php

namespace Controllers;

/**
 * Description of Directories
 *
 * @author alex
 */
class Directories extends \Controllers\BaseController {

    private $directoriesModel;

    public function __construct($app) {
        parent::__construct($app);
        $this->directoriesModel = $this->app->getModel('DirectoriesModel');
    }

    public function index() {

        $this->main();
    }

    public function main() {
        $this->title = 'Справочники';
        $this->setContentTemplate('Content/directories-list.html');
        $contentPart = $this->page->getPagePart('content');
        $contentPart->set('directories', $this->directoriesModel->getDirectories());
        $this->pageModel->addScript("directories-list.js");
        $this->output();
    }

    public function lol() {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP1Object');
        for ($i = 0; $i < 400; $i++) {
            $dbObject->insert(['Fio' => 'lol' . $i]);
        }
        $this->main();
    }

    public function edit() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        $spItemId = $this->app->getModule('Request')->getParameter('i');
        if ($spItemId) {
            $this->showEditDirectoryItem($dirSP, $spItemId);
        } else {
            $this->showEditDirectory($dirSP); 
        }
    }

    public function save() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        $spItemId = $this->app->getModule('Request')->getParameter('i');
        $mode = $this->app->getModule('Request')->getParameter('mode');
        if ($spItemId) {
            if ($mode == "edit") {
                $this->directoriesModel->updateDirectoryItem($dirSP, $spItemId);
            } else if ($mode == "new") {
                $this->directoriesModel->addDirectoryItem($dirSP, $spItemId);
            }
            $this->show();
        } else {
            if ($dirSP) {
                
            } else {

                $attrs = [];
                $parametrs = $this->app->getModule('Request')->getParameters();
                $dirName = $parametrs['directory-name'];
                $dirSP = $parametrs['directory-number'];

                foreach ($parametrs as $key => $value) {
                    if (strpos($key, 'attrid') !== false) {
                        $attrId = str_replace("attrid", "", $key);

                        $attrs[$attrId] = ['attrname' => $parametrs['attrname' . $attrId],
                            'attrtype' => $parametrs['attrtype' . $attrId]];
                    }
                }

                $this->directoriesModel->addDirectory($dirName, $dirSP, $attrs);
            }
            $this->main();
        }
    }

    public function add() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        if ($dirSP) {
            $this->showAddDirectoryItem($dirSP);
        } else {
            $this->showAddDirectory();
        }
    }

    public function remove() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        $spItemId = $this->app->getModule('Request')->getParameter('i');
        $rmtype = $this->app->getModule('Request')->getParameter('rmtype');
        if ($rmtype == 'item') {
            if ($spItemId) {
                $this->directoriesModel->removeDirectoryItem($dirSP, $spItemId);
            } else {

                $spItemsId = [];
                $parameters = $this->app->getModule('Request')->getParameters();

                foreach ($parameters as $key => $value) {
                    if (strpos($key, 'item') !== false) {
                        $spItemId = str_replace("item", "", $key);
                        $spItemsId[] = $spItemId;
                    }
                }
                $this->directoriesModel->removeDirectoryItems($dirSP, $spItemsId);
            }
            $this->show();
        } else if ($rmtype == 'dir') {
            if ($dirSP) {
                $this->directoriesModel->removeDirectoryBySP($dirSP);
            } else {
                $dirsSP = [];
                $parameters = $this->app->getModule('Request')->getParameters();

                foreach ($parameters as $key => $value) {
                    if (strpos($key, 'directory') !== false) {
                        $dirSP = str_replace("directory", "", $key);
                        $dirsSP[] = $dirSP;
                    }
                }
                $this->directoriesModel->removeDirectory($dirsSP);
            }
            $this->main();
        }
    }

    public function removeall() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        $this->directoriesModel->removeAllItems($dirSP);
        $this->show();
    }

    public function show() {
        $dirSP = $this->app->getModule('Request')->getParameter('sp');
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');

        if ($dbObject) {
            $directoryInfo = $this->directoriesModel->getDirectoryInfo($dirSP);
            $this->title = $directoryInfo['Title'];

            $this->setContentTemplate('Content/directory-item-list.html');
            $contentPart = $this->page->getPagePart('content');
            $contentPart->set('attrs', $dbObject->getAttrs());

            $result = $dbObject->select();

            $content = [];
            while ($row = $result->next()) {
                $row['Url'] = "/directories/edit?sp=" . $dirSP . "&i=" . $row['ID'];
                $content[] = $row;
            }

            $contentPart->set('content', $content);
            $contentPart->set('dirSP', $dirSP);
            $this->pageModel->addScript("directory-item-list.js");
        }

        $this->output();
    }

    public function showAddDirectoryItem($dirSP) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');

        if ($dbObject) {
            $this->title = "Редактирование элемента";

            $this->setContentTemplate('Content/directory-item-edit.html');
            $contentPart = $this->page->getPagePart('content');

            $contentPart->set('objectAttrs', $this->app->getModule('DBObjectManager')->getEmptyHtml($dbObject));
            $contentPart->set('prevUrl', '/directories/show?sp=' . $dirSP);
            $contentPart->set('sp', $dirSP);
            $contentPart->set('mode', 'new');
            $this->pageModel->addScript("directory-item-edit.js");
        }

        $this->output();
    }

    public function showEditDirectoryItem($dirSP, $spItemId) {
        $dbObject = $this->app->getModule('DBObjectManager')->getDBObject('SP' . $dirSP . 'Object');

        if ($dbObject) {
            $this->title = "Редактирование элемента";

            $this->setContentTemplate('Content/directory-item-edit.html');
            $contentPart = $this->page->getPagePart('content');

            $dbObject->select(['where' => ['ID = ?i', $spItemId]]);

            $contentPart->set('objectAttrs', $this->app->getModule('DBObjectManager')->getHtml($dbObject));
            $contentPart->set('prevUrl', '/directories/show?sp=' . $dirSP);
            $contentPart->set('sp', $dirSP);
            $contentPart->set('i', $spItemId);
            $contentPart->set('mode', 'edit');
            $this->pageModel->addScript("directory-item-edit.js");
        }

        $this->output();
    }

    public function showAddDirectory() {
        $this->title = "Редактирование справочника";

        $this->setContentTemplate('Content/directory-add.html');
        $contentPart = $this->page->getPagePart('content');
        $contentPart->set('prevUrl', '/directories');
        $this->pageModel->addScript("directory-add.js");

        $this->output();
    }

    public function showEditDirectory($dirSP) {
        
    }

}
