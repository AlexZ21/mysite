<?php

namespace Controllers;

/**
 * Description of Controller
 *
 * @author alex
 */
class BaseController extends \Core\Component {

    protected $pageManager;
    protected $response;
    protected $page;
    protected $pageModel;
    protected $title;

    public function __construct(&$app) {
        parent::__construct($app);
        $this->pageManager = $this->app->getModule('PageManager');
        $this->response = $this->app->getModule('Response');
        $this->page = $this->pageManager->getPage();
        $this->pageModel = $this->app->getModel('PageModel');
        $this->pageModel->setPage($this->page);
    }

    public function index() {
        
    }
    
    public function setContentTemplate($templatePath) {
        $contentPart = &$this->page->getPagePart('content');
        $contentPart->setTemplatePath($templatePath);        
    }

    public function output() {
        $this->pageModel->setMenuTitle($this->title);        
        $this->pageModel->makePage();        
        $this->response->write($this->pageManager->getHtml($this->page));
    }

}
