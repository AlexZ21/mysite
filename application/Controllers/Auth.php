<?php

namespace Controllers;

/**
 * Description of CAuth
 *
 * @author alex
 */
class Auth extends \Controllers\BaseController {

    function __construct($app) {
        parent::__construct($app);
        $this->userManager = $this->app->getModule("UserManager");
        $this->user = &$this->userManager->getCurrentUser();
        if (!$this->user) {
            $this->request = &$this->app->getModule("Request");
            if (!strlen($this->request->getParameter("login")) ||
                !strlen($this->request->getParameter("password"))) {
                    $this->makeAuthPage();
                    $this->output();
            } else {
                $this->enter();
            }
        }
    }

    public function index() {
        
    }

    public function enter() {
        $user = $this->userManager->exist(
                $this->request->getParameter("login"), $this->request->getParameter("password"), $this->request->getParameter("sec"));

        if ($user) {
            $this->userManager->authUser($user);
            $this->app->executeDefaultController();
        }else{
            $this->makeAuthPage();   
            $this->output();            
        }
        
    }
    
    public function logout() {
        $this->userManager->logoutCurrentUser();
        $this->makeAuthPage();
        $this->output();
    }

    
    public function makeAuthPage() {
        $this->setContentTemplate("Content/auth.html");
        $this->page->setOnlyContent(TRUE);
        $this->title = "Авторизация";
        $contentPart = &$this->page->getPagePart('content');
        $contentPart->set("secr", $this->userManager->generateSecretString());
    }
    
    public function userInfo() {
        echo $this->user->getAttr("ID") . "<br>";
        echo $this->user->getAttr("Login") . "<br>";
        echo $this->user->getAttr("Password") . "<br>";
        echo $this->user->getAttr("Email") . "<br>";
        echo $this->user->getAttr("GroupID") . "<br>";
        echo $this->user->getAttr("GroupTitle") . "<br>";
    }

}
