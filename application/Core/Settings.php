<?php

namespace Core;

/**
 * Description of Settings
 *
 * @author alex
 */
class Settings {

    private $file;
    private $settings = [];

    public function __construct($file) {
        $this->file = $file;
        $jsonString = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/" . $this->file);
        $this->settings = json_decode($jsonString, TRUE);
    }

    public function get($name) {
        if (isset($this->settings[$name])) {
            return $this->settings[$name];
        } else {
            return NULL;
        }
    }

    public function set($name, $value) {
        $this->settings[$name] = $value;
    }

}
