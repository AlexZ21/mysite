<?php

namespace Core;

/**
 * Description of Util
 *
 * @author alex
 */
class Util{

    public function __construct($app) {
        parent::__construct($app);
   }

    static public function getEscapeValue($value, &$conn) {
        $refConn = &$conn;
        if (is_null($value)) {
            return "NULL";
        }
        if (is_numeric($value)) {
            if (is_double($value)) {
                return Util::escapeDouble($value);
            }
            return Util::escapeInt($value);
        }
        if (is_string($value)) {
            return Util::escapeString($value, $refConn);
        }
        return "NULL";
    }

    static function escapeInt($value) {
        if ($value === NULL) {
            return 'NULL';
        }
        if (!is_numeric($value)) {
            throw new Ex("Не правильный тип параметра ".$value);
        }
        if (is_float($value)) {
            $value = number_format($value, 0, '.', '');
        }
        return $value;
    }

    static function escapeDouble($value) {
        if ($value === NULL) {
            return 'NULL';
        }
        if (!is_numeric($value)) {
            throw new Ex("Не правильный тип параметра ".$value);
        }
        return $value;
    }

    static function escapeString($value, &$conn) {
        if ($value === NULL) {
            return 'NULL';
        }
        $refConn = &$conn;
        return "'" . mysqli_real_escape_string($refConn, $value) . "'";
    }

}
