<?php

namespace Core;

/**
 * Шаблон модуля.
 *
 * @author alex
 */
class Module extends Component {

    protected $settings = [];
    protected $dir;

    /**
     * Конструктор.
     * 
     * @param Application $app Ссылка на экземпляр приложения.
     */
    public function __construct(&$app) {
        parent::__construct($app);
        $this->initModule();
    }

    protected function initModule() {
        $appSettings = &$this->app->getSettings();

        $namespace = substr(get_class($this), 0, strrpos(get_class($this), '\\'));
        $this->dir = $appSettings->get('appDir').'/'.str_replace('\\', '/', $namespace);

        $settingsPath = $this->dir . "/settings.json";

        if (file_exists($settingsPath)) {
            $jsonString = file_get_contents($settingsPath);
            $this->settings = json_decode($jsonString, TRUE);
        }

        $fromAppSettings = &$appSettings->get(get_class($this));
        if (isset($fromAppSettings)) {
            $this->settings = $this->settings + $fromAppSettings;
        }

        if (isset($this->settings['includeDir'])) {
            foreach ($this->settings['includeDir'] as $dir) {
                $this->app->addIncludeDir($this->dir . "/" . $dir);
            }
        }

    }
}

?>