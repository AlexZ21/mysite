<?php

namespace Core;

/**
 * Класс для работы с исключениями
 *
 * @author alex
 */
class Ex extends \Exception {

    /**
     * Вывод текста исключения.
     */
    public function printError() {
        echo '<html>
              <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Ошибка</title>
              </head>
              <body><div style="color: #F00;">ОШИБКА</div>
              <div>Код: <b>' . $this->getCode() . '</b></div>
              <div>Сообщение: <b>' . $this->getMessage() . '</b></div>
              <div>Строка: <b>' . $this->getLine() . '</b></div>
              <div>Файл: <b>' . $this->getFile() . '</b></div>
              <div>Стек вызывов: <b>' . $this->getTraceAsString(). '</b></div>
              
              </body>';
        exit;
    }

}

?>
