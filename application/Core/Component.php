<?php

namespace Core;

/**
 * Description of Component
 *
 * @author alex
 */
class Component {
    
    protected $app;

    public function __construct(&$app) {
        $this->app = $app;
    }
    
    public function getApplication() {
        return $this->app;
    }
    
}
