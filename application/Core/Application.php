<?php

namespace Core;

require_once 'Settings.php';

/**
 * Главный класс приложения. Хранит в себе массив модулей.
 *
 * @author alex
 */
class Application {

    private $run = FALSE;
    private $modules = [];
    private $settings = NULL;
    private $moduleManager;

    public function __construct() {
        $this->settings = new Settings("application/settings.json");
        $this->registerAutoloads();

        $this->moduleManager = new ModuleManager($this);
    }

    public function __destruct() {
        
    }

    public function getSettings() {
        return $this->settings;
    }

    /**
     * Загрузка модуля.
     * 
     * @param string $name Название модуля.
     * @param array $args Массив аргументов для передачи в конструктор модуля.
     * @throws Ex
     */
    public function loadModule($name) {
        return $this->moduleManager->loadModule($name);
    }

    /**
     * Получение модуля.
     * 
     * @param string $name Название модуля.
     * @return Module|null Если модуль не загружен, то возвращается null;
     */
    public function getModule($name) {
        return $this->moduleManager->getModule($name);
    }

    public function getModel($name) {
        $name = '\\Models\\' . $name;
        return new $name($this);
    }
    
    public function getController($name) {
        $name = '\\Controllers\\' . $name;
        return new $name($this);        
    }

    /**
     * Звапускает приложение.
     */
    public function run() {
        if (!$this->run) {
            $this->run = TRUE;

            if ($this->getModule('UserManager')->isUserAuth()) {

                $user = $this->getModule('UserManager')->getFromSession();
                $this->getModule('UserManager')->setCurrentUser($user);
 
                $routes = explode('/', $this->getModule('Request')->getRequestUri());

                if ($routes[1]) {
                    $class = $routes[1];

                    if ($class == "index.php") {
                        $class = $this->settings->get('defaultController');
                    }

                    $method = NULL;

                    if (isset($routes[2])) {
                        $routes[2] = explode('?',$routes[2]);
                        $method = $routes[2][0];
                    }

                        $this->executeController($class, $method);
                } else {
                    $this->executeController($this->settings->get('defaultController'));
                }
            } else {
                $this->executeController('Auth');
            }
        }
    }

    /**
     * Запускает контроллер. Контроллер является модулем. Название контроллера и
     * метод берутся из ссылки. Например: test.com/class/method
     * 
     * @param string $class Название класса.
     * @param string $method Метод класса.
     * @throws Ex
     */
    public function executeController($class, $method = NULL) {
        $class = ucfirst($class);
        $controller = $this->getController($class);
        if ($method) {
            if (method_exists($controller, $method)) {
                $controller->$method();
            } else {
                throw new Ex("Метод ".$method." не найен");
            }
        } else {
            $controller->index();
        }
    }

    public function executeDefaultController() {
        $this->executeController($this->settings->get('defaultController'));
    }

    public function addIncludeDir($folder) {
        $includeDir = &$this->settings->get('includeDir');
        $includeDir[] = $folder;
        $this->settings->set('includeDir', $includeDir);
    }

    private function registerAutoloads() {
        return spl_autoload_register(
                function ($classname) {
            $file = str_replace('\\', "/", $classname) . '.php';
            $founded = FALSE;
            foreach ($this->settings->get('includeDir') as $value) {
                $rFile = $value . "/" . $file;
                if (is_file($rFile)) {
                    require_once $rFile;
                    $founded = TRUE;
                    break;
                }
            }
            if (!$founded) {
                throw new Ex("Класс " . $classname . " не найден");
            }
        }
        );
    }

}
