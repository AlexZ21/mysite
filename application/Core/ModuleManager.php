<?php

namespace Core;

/**
 * Description of ModuleManager
 *
 * @author alex
 */
class ModuleManager {

    private $modules = [];
    private $app;
    private $appSettings;

    public function __construct(&$app) {
        $this->app = &$app;
        $this->appSettings = $app->getSettings();
    }

    public function setAppSettings(&$settings) {
        $this->appSettings = &$settings;
    }

    public function getDir($name) {
        return $this->appSettings->get('modulesDir') . "/" . $name;
    }

    public function loadModule($name) {
        $dir = $this->getDir($name);
        if (file_exists($dir)) {
            //require_once $dir . "/" . $name . ".php";
            
            $class = '\\Modules\\' . $name . '\\' . $name;            
            $module = new $class($this->app);
            $this->modules[$name] = $module;
            return $this->modules[$name];
        } else {
            throw new Ex("Модуль " . $name . " не наден");
        }
    }

    public function getModule($name) {
        if (isset($this->modules[$name])) {
            return $this->modules[$name];
        } else {
            return $this->loadModule($name);
        }
    }

}

?>