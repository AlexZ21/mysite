<?php

namespace Modules\PageManager;

class PagePartTemplate {

    private $templatePath;
    private $vars = [];
    
    public function __construct($templatePath) {
        $this->templatePath = $templatePath;
        $this->vars['show'] = 1;
    }
    
    public function setTemplatePath($templatePath) {
        $this->templatePath = $templatePath;
    }
    
    public function getTemplatePath() {
        return $this->templatePath;
    }
    
    public function setVars($vars) {
        $this->vars = $vars;
    }
    
    public function &getVars() {
        return $this->vars;
    }

    public function set($key, $value) {
        $this->vars[$key] = $value;
    }

}

/**
 * Description of Page
 *
 * @author alex
 */
class Page {

    private $title;
    private $page;
    private $pageParts = [];
    private $theme = "Default";
    private $onlyContent = FALSE;

    public function __construct() {
        $this->setPageParts();
    }

    private function setPageParts() {
        $this->page = new PagePartTemplate("Frame/page.html");//"Frame/page.html";
        $this->pageParts['meta'] = new PagePartTemplate("Frame/meta.html");
        $this->pageParts['header'] = new PagePartTemplate("Frame/header.html");
        $this->pageParts['footer'] = new PagePartTemplate("Frame/footer.html");
        $this->pageParts['left'] = new PagePartTemplate("Frame/left.html");
        $this->pageParts['right'] = new PagePartTemplate("Frame/right.html");
        $this->pageParts['content'] = new PagePartTemplate("Frame/content.html");
    }
    
    public function getPageParts() {
            return $this->pageParts;
    }
    
    public function getPagePart($name) {
        if (isset($this->pageParts[$name])) {
            return $this->pageParts[$name];
        } else {
            return NULL;
        }
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function setOnlyContent($mode) {
        $this->onlyContent = $mode;
    }
    
    public function getOnlyContent() {
        return $this->onlyContent;
    }

}