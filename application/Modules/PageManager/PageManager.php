<?php

namespace Modules\PageManager;

/**
 * Description of PageManager
 *
 * @author alex
 */
class PageManager extends \Core\Module {

    private $templater;
    private $theme = "Default";
    private $themeSettings = NULL;

    public function __construct($app) {
        parent::__construct($app);
        $settings = &$app->getSettings();
        $this->theme = $settings->get('theme');
        $this->templater = &$this->app->getModule('Templater');
        $this->loadThemeSettings();
    }
    
    public function loadThemeSettings() {
        $settings = &$this->app->getSettings();   
        $themeSettingsPath = $settings->get('templateDir') . "/" . $this->theme . "/theme-settings.json";
        if (file_exists($themeSettingsPath)) {
            $jsonString = file_get_contents($themeSettingsPath);
            $this->themeSettings = json_decode($jsonString, TRUE);
        }
    }

    public function getPage() {
        $page = new Page();
        $page->setTheme($this->theme);
        return $page;
    }
    
    public function &getThemeSettings() {
        return $this->themeSettings;
    }

    /**
     * 
     * @param Page $page
     */
    public function getHtml(&$page) {
        $vars = [];
        foreach ($page->getPageParts() as $key => $pagePart) {
            
            $vars[$key] = $this->templater->getTemplate("/".$this->theme."/".
                    $pagePart->getTemplatePath());
            
            if($this->themeSettings) {
                if(isset($this->themeSettings[$key])) {
                    $pagePartVars = &$pagePart->getVars();
                    $pagePartVars = $pagePartVars + $this->themeSettings[$key];

                }
            }
            
            if($page->getOnlyContent() && $key != 'content') {
                $pagePart->set('show', 0);
            }

            $vars[$key]->setVars($pagePart->getVars());
            $vars[$key] = $vars[$key]->fetch();
        }
        
        $vars['title'] = $page->getTitle();
        
        $pageTemplate = $this->templater->getTemplate("/".$this->theme."/Frame/page.html");
        $pageTemplate->setVars($vars);
        return $pageTemplate->fetch();       
    }

}
