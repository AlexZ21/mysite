<?php

namespace Modules\DBManager;

/**
 * Класс для работы с результатами запроса.
 */
class DBResult {

    private $result = NULL;
    private $currentRow = NULL;

    
    /**
     * Конструктор.
     * 
     * @param mysqli $result mysqli результат запроса.
     */
    public function __construct(&$result) {
        $this->result = &$result;
    }
    
    
    /**
     * Возвращает количество найденых строк.
     * 
     * @return int 
     */
    function rowCount() {
       return ($this->result) ? $this->result->num_rows : 0; 
    }

    
    /**
     * Возвращает следующую строку в найденых.
     * 
     * @return array|null 
     */
    function next() {
        return ($this->result) ? $this->currentRow = $this->result->fetch_assoc() : NULL;
    }

    
    /**
     * Получает атрибут текущей строки.
     * 
     * @param string $attr Название атрибута.
     * @return mixed
     */
    function get($attr) {
        return $this->currentRow[$attr];
    }

}


/**
 * Простой класс для работы запросами.
 */
class DBStatement {

    protected $conn = NULL;

    
    /**
     * Конструктор.
     * 
     * @param mysqli $conn mysqli подключение.
     */
    public function __construct(&$conn) {
        $this->conn = &$conn;
    }


    
    /**
     * Выполнение запроса.
     * 
     * @param string $query Запрос.
     * @return \DBResult Результат запроса.
     * @throws Ex
     */
    public function executeQuery($query) {
        if ($this->conn) {
            $result = $this->conn->query($query);
            if (mysqli_errno($this->conn)) {
                throw new \Core\Ex(mysqli_error($this->conn), mysqli_errno($this->conn));
            }
            return new DBResult($result);
        }
    }

}


/**
 * Класс для работы с подготовленными запросами.
 */
class DBPreparedStatement extends DBStatement {

    private $query;
    private $values = [];
    private $util;

    
    /**
     * Конструктор.
     * 
     * @param mysqli $conn mysqli соединение.
     * @param string $query Запрос.
     */
    public function __construct(&$conn, $query) {
        parent::__construct($conn);
        $this->query = $query;
    }

    
    /**
     * Добавляет значение в запрос.
     * 
     * @param int $i Индекс.
     * @param mixed $value Значение.
     */
    public function bind($i, $value) {
        $this->values[$i] = $value;
    }
    
    public function bindsArray($values) {
        $this->values = $values;
    }

    
    /**
     * Выполняет запрос.
     * 
     * @return DBResult Результат запроса.
     * @throws Ex
     */
    public function execute() {
        $preparedQuery = "";
        $arr = preg_split('~(\?[nsiuap])~u', $this->query, null, PREG_SPLIT_DELIM_CAPTURE);

        $valueIndex = 1;

        foreach ($arr as $i => $part) {
            if (($i % 2) == 0) {
                $preparedQuery .= $part;
            } else {
                if (!array_key_exists($valueIndex, $this->values)) {
                    throw new \Core\Ex("Отсутствует ".$valueIndex." параметр");
                }
                switch ($part) {
                    case '?s':
                        $part = \Core\Util::escapeString($this->values[$valueIndex], $this->conn);
                        break;
                    case '?i':
                        $part = \Core\Util::escapeInt($this->values[$valueIndex]);
                        break;
                    case '?d':
                        $part = \Core\Util::escapeDouble($this->values[$valueIndex]);
                        break;
                }
                $valueIndex++;
                $preparedQuery .= $part;
            }
        }
        return $this->executeQuery($preparedQuery);
    }

}


/**
 * Класс для работы с базой данных. Является модулем и
 * унаследован от класса Module.
 *
 * @author alex
 */
class DBManager extends \Core\Module {

    private $conn = NULL;
    private $isConnected = FALSE;

    
    /**
     * Конструктор. Параметры для соединения берутся из настроек.
     * 
     * @param Application $app Экземпляр приложения
     * @throws Ex
     */
    public function __construct($app) {
        parent::__construct($app);
        $appSettings = &$app->getSettings();
        
        $this->settings['host'] = $appSettings->get('dbHost');
        $this->settings['user'] = $appSettings->get('dbUser');
        $this->settings['pass'] = $appSettings->get('dbPassword');
        $this->settings['db'] = $appSettings->get('db');
        $this->settings['charset'] = 'utf8';

        @ $this->conn = new \mysqli($this->settings['host'], $this->settings['user'], $this->settings['pass'], $this->settings['db']);
        
        if (mysqli_connect_errno()) {
            throw new \Core\Ex(mysqli_connect_error(), mysqli_connect_errno());
        } else {
            $this->conn->set_charset($this->settings['charset']);
            $this->isConnected = TRUE;
        }
    }

    /**
     * Создает экземпляр класса для работы с простыми запросами к базе данных.
     * 
     * @return DBStatement
     */
    public function createStatement() {
        return ($this->isConnected) ? new DBStatement($this->conn) : FALSE;
    }

    
    /**
     * Создает экземпляр класса для работы с подготовленными запросами к базе
     * данных.
     * 
     * @param string $query Запрос.
     * @return DBPreparedStatement
     */
    public function prepareStatement($query) {
        return ($this->isConnected) ? new DBPreparedStatement($this->conn, $query) : FALSE;
    }

    
    /**
     *Возвращает mysqli соединение.
     * 
     * @return mysqli
     */
    public function getConnection() {
        return ($this->isConnected) ? $this->conn : FALSE;
    }

    
    /**
     * Возвращает таблицу из базы данных в виде объекта. 
     * 
     * @param string $tableName Название таблицы.
     * @return \Table Объект таблицы.
     */
    public function getTable($tableName) {
        $pStmt = $this->prepareStatement("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
                               WHERE TABLE_SCHEMA='" . $this->settings['db'] . "' AND TABLE_NAME=?s");
        $pStmt->bind(1, $tableName);
        $result = $pStmt->execute();

        $attr = [];
        while ($result->next()) {
            $attr[] = $result->get('COLUMN_NAME');
        }
        return new Table($tableName, $attr, $this);
    }

    
    /**
     * Закрывает соединение.
     */
    public function close() {
        if ($this->isConnected) {
            $this->conn->close();
            $this->isConnected = FALSE;
        }
    }

    
    /**
     * Деструктор.
     */
    public function __destruct() {
        $this->close();
    }

}
