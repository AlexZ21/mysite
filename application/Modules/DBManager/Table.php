<?php

namespace Modules\DBManager;

/**
 * Description of Table
 *
 * @author alex
 */
class Table {

    private $name;
    private $attr = [];
    private $dbManager = NULL;

    public function __construct($name, $attr, &$dbManager) {
        $this->name = $name;
        $this->attr = $attr;
        $this->dbManager = &$dbManager;
    }

    public function insert($query) {
        $sql = "INSERT INTO " . $this->name . " (";
        $binds = [NULL];


        $columns = $query["columns"];
        $values = array_shift($query["values"]);
        if (count($query["values"])) {
            $binds = array_merge($binds, $query["values"]);
        }

        $sql = $sql . $columns . ") VALUES (" . $values . ")";
        
        

        $pStmt = $this->dbManager->prepareStatement($sql);
        $pStmt->bindsArray($binds);
        $pStmt->execute();
    }

    public function update($query) {
        $sql = "UPDATE " . $this->name;
        $set = " SET ";
        $where = " ";
        $binds = [NULL];

        foreach ($query as $key => $value) {
            switch ($key) {
                case "set":
                    $set .= array_shift($query['set']);
                    if (count($query['set'])) {
                        $binds = array_merge($binds, $query['set']);
                    }
                    break;
                case "where":
                    $where .= " WHERE ";
                    $where .= array_shift($query['where']);
                    if (count($query['where'])) {
                        $binds = array_merge($binds, $query['where']);
                    }
                    break;
            }
        }
        $sql = $sql . $set . $where;
        $pStmt = $this->dbManager->prepareStatement($sql);
        $pStmt->bindsArray($binds);
        $pStmt->execute();
    }

    public function remove($query) {
        $binds = [NULL];
        $sql = "DELETE FROM " . $this->name;
        $where = "";
        if (isset($query['where'])) {
            $where .= " WHERE ";
            $where .= array_shift($query['where']);

            if (count($query['where'])) {
                $binds = array_merge($binds, $query['where']);
            }

            $sql = $sql . $where;
            $pStmt = $this->dbManager->prepareStatement($sql);
            $pStmt->bindsArray($binds);
            $pStmt->execute();
        }
    }

    public function select($query) {
        $sql = "SELECT ";
        $columns = " * ";
        $where = " ";
        $binds = [NULL];
        $order = "";
        $limit = "";

        foreach ($query as $key => $value) {
            switch ($key) {
                case "columns":
                    $comma = "";
                    $columns = "";
                    foreach ($value as $column) {
                        if (in_array($column, $this->attr)) {
                            $columns .= $comma . $column;
                            $comma = ",";
                        }
                    }
                    break;
                case "where":
                    $where .= " WHERE ";
                    $where .= array_shift($query['where']);
                    if (count($query['where'])) {
                        $binds = array_merge($binds, $query['where']);
                    }

                    break;

                case "orderby":
                    $order = " ORDER BY ";
                    $comma = "";
                    foreach ($value as $ord) {
                        $order .= $comma . $column;
                        $comma = ",";
                    }
                    break;

                case "limit":
                    $limit = " LIMIT ?i ";
                    $binds[] = $value;
                    break;

                default:
                    break;
            }
        }

        $sql = $sql . $columns . " FROM " . $this->name . $where . $order . $limit;

        $pStmt = $this->dbManager->prepareStatement($sql);
        $pStmt->bindsArray($binds);
        $result = $pStmt->execute();

        return $result;
    }
    
    public function truncate() {
        $stmt = $this->dbManager->createStatement();
        $stmt->executeQuery("TRUNCATE ".$this->name);
    }

}
