<?php

namespace Modules\UserManager;

/**
 * Description of User
 *
 * @author alex
 */
class User {
    private $dbManager;
    private $attr = [];

    public function __construct() { 
    }

    public function auth() {
        if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
        }
        $_SESSION['ID'] = $this->getAttr("ID");
    }

    public function getAttr($name) {
        if (isset($this->attr[$name])) {
            return $this->attr[$name];
        } else {
            return FALSE;
        }
    }
    
    public function setAttr($name, $value) {
        $this->attr[$name] = $value;
    }
}


 class UserManager extends \Core\Module {
     
    private $currentUser;
     
    function __construct($app) {
        parent::__construct($app);
    }
    
    public function authUser(&$user) {
        $this->currentUser = $user;
                if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
        }
        $_SESSION['ID'] = $this->currentUser->getAttr("ID");
    }
    
    public function logoutCurrentUser() {
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_destroy();
        }
    }
    
    public function setCurrentUser($currentUser) {
        $this->currentUser = $currentUser;
    }
    
    public function getCurrentUser() {
        return $this->currentUser;
    }
    
    public function getFromSession() {
        if ($this->isUserAuth()) {
            return $this->getById($_SESSION['ID']);
        } else {
            return FALSE;
        }
    }

    public function getById($id) {
        $dbManager = &$this->app->getModule('DBManager');
        $request = &$this->app->getModule('Request');

        $pStmt = &$dbManager->prepareStatement("SELECT U.ID AS ID, "
                . "U.Login AS Login, "
                . "U.Password AS Password, "
                . "U.Name AS Name, "
                . "U.Surname AS Surname, "
                . "U.GroupID AS GroupID, "
                . "G.Title AS GroupTitle "
                . "FROM Users AS U "
                . "LEFT JOIN Groups AS G ON G.ID = U.GroupID " 
                ." WHERE U.ID = ?i");
        $pStmt->bind(1, $id);

        $result = $pStmt->execute();

        if ($result->rowCount() > 0) {
            $user = new User();
            while ($row = $result->next()) {
                foreach ($row as $name => $value) {
                    $user->setAttr($name, $value);
                }
            }
            return $user;
        } else {
            return FALSE;
        }
    }
    
    public function isUserAuth() {
        $request = &$this->app->getModule('Request');
        if ($request->getCookie('PHPSESSID')) {
            if (session_status() != PHP_SESSION_ACTIVE) {
                session_start();
            }
            if (isset($_SESSION['ID'])) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
    
    public function exist($login, $password, $sec) {
        $dbManager = &$this->app->getModule('DBManager');
        $request = &$this->app->getModule('Request');

        $pStmt = &$dbManager->prepareStatement("SELECT * FROM Users WHERE Login = ?s"
                . " AND SHA1(CONCAT(Password, ?s)) = ?s");
        $pStmt->bind(1, $request->getParameter('login'));
        $pStmt->bind(2, $request->getParameter('sec'));
        $pStmt->bind(3, $request->getParameter('password'));

        $result = $pStmt->execute();

        if ($result->rowCount() > 0) {
            $user = new User();
            while ($row = $result->next()) {
                foreach ($row as $name => $value) {
                    $user->setAttr($name, $value);
                }
            }
            return $user;
        } else {
            return FALSE;
        }
    }
    
    public function generateSecretString() {
        $request = &$this->app->getModule('Request');
        $securityHash = sha1(time() . rand() . $request->getRemoteAddr());
        return $securityHash;
    }
    
 }

?>
