<?php

namespace Modules\Response;

/**
 * Класс для обработки ответа клиенту
 *
 * @author alex
 */
class Response extends \Core\Module {
    private $appSettings;
    private $respString = "";


    public function __construct($app) {
        parent::__construct($app);
        $this->appSettings = &$this->app->getSettings();
    }
    
    public function __destruct() {
        $this->output();
    }
    
    public function output() {
        print $this->respString;
    }
    
    public function write($string) {
        $this->respString .= $string;
    }
    
}
