<?php

namespace Modules\Request;

/**
 * Класс для обработки запроса от клиента
 *
 * @author alex
 */
class Request extends \Core\Module {

    private $params = array();

    public function __construct($app) {
        parent::__construct($app);
        if (filter_input_array(INPUT_POST)) {
            $this->params = array_merge($this->params, filter_input_array(INPUT_POST));
        }
        if (filter_input_array(INPUT_GET)) {
            $this->params = array_merge($this->params, filter_input_array(INPUT_GET));
        }
    }

    public function getCookie($name) {
        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        } else {
            return FALSE;
        }
    }

    public function getParameter($param) {
        if (isset($this->params[$param])) {
            return $this->params[$param];
        } else {
            return FALSE;
        }
    }

    public function &getParameters() {
        return $this->params;
    }

    public function getRemoteAddr() {
        return $_SERVER['REMOTE_ADDR'];
    }

    public function getRequestUri() {
        return $_SERVER['REQUEST_URI'];
    }

}

?>
