<?php

namespace Modules\Templater;

require_once 'Fenom/Fenom.php';

class Template {

    private $template;
    private $vars = [];

    function __construct(&$fenomTemplate) {
        $this->template = &$fenomTemplate;
    }
    
    public function setVars($vars) {
        $this->vars = $vars;
    }

    public function set($key, $value) {
        $this->vars[$key] = $value;
    }

    public function fetch() {
        return $this->template->fetch($this->vars);
    }

}

/**
 * Description of Templater
 *
 * @author alex
 */
class Templater extends \Core\Module {

    private $fenom;
    private $vars = [];
    private $templates = [];

    public function __construct($app) {
        parent::__construct($app);
        $settings = &$app->getSettings();
        $this->fenom = \Fenom::factory($settings->get('templateDir'), "cache", \Fenom::FORCE_COMPILE);
    }

    public function getTemplate($name) {
        $template = &$this->fenom->getTemplate($name);
        $this->templates[] = &$template;
        return new Template($template);
    }

}

?>