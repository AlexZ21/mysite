<?php

namespace Modules\DBObjectManager;

/**
 * Description of DbObject
 *
 * @author alex
 */
class DBObject {
    
    private $table;
    private $attrs = [];
    private $objectId;
    private $dbManager = NULL;
    private $result = NULL;
    


    public function __construct($table, $objectId, &$attrs, &$dbManager) {
        $this->table = $table;
        $this->objectId = $objectId;
        $this->attrs = $attrs;
        $this->dbManager = $dbManager;
    }
    
    public function &getAttrs() {
        return $this->attrs;
    }
    
    public function select($query = []) {
        $table = $this->dbManager->getTable($this->table);
        $columns = [];
        
        foreach ($this->attrs as $value) {
            $columns[] = $value['name'];
        }
        
        $query['columns'] = $columns;
        
        $this->result = $table->select($query);
        return $this->result;
    }
    
    public function getResult() {
        return $this->result;
    }
    
    public function update($values) {
        $table = $this->dbManager->getTable($this->table);
        $table->update($values);
    }
    
    public function insert($values) {
        $query = [];
        $query['columns'] = "ID";
        $query['values'] = [];
        $query['values'][0] = "NULL";
        $comma = ",";
        foreach ($this->attrs as $attr) {
            if(isset($values[$attr['name']])) {
                $query['columns'] .= $comma . $attr['name'];
                $query['values'][0] .= $comma . "?" . $attr['type'][0];
                $query['values'][] = $values[$attr['name']];
            }
        }
        
        $table = $this->dbManager->getTable($this->table);
        $table->insert($query);
    }
    
    public function remove($values) {
        $table = $this->dbManager->getTable($this->table);
        $table->remove($values);        
    }
    
    public function removeById($id) {
        $this->remove(['where' => ["ID = ?i", $id]]);
    }
    
    public function removeAll() {
        $table = $this->dbManager->getTable($this->table);
        $table->truncate();          
    }
        
}
