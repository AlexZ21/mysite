<?php

namespace Modules\DBObjectManager;

/**
 * Description of ObjectManager
 *
 * @author alex
 */
class DBObjectManager extends \Core\Module {

    private $types = [
        'number-edit' => ['i','varchar'],
        'number' => ['i', 'int'],
        'string-edit' => ['s', 'varchar']
    ];

    public function __construct($app) {
        parent::__construct($app);
    }

    public function getDBObject($name) {
        $objTable = $this->app->getModule('DBManager')->getTable('Objects');
        $result = $objTable->select(['columns' => ['ID', 'DbTable'], 'where' => ['Name LIKE ?s', $name]]);

        if ($result->rowCount()) {
            $objAttrTable = $this->app->getModule('DBManager')->getTable('ObjectAttributes');
            $result->next();
            $objectId = $result->get('ID');
            $table = $result->get('DbTable');
            $result = $objAttrTable->select(['where' => ['ObjectId = ?i', $objectId]]);

            if ($result->rowCount()) {
                $attrs = [];
                while ($result->next()) {
                    $attrs[] = ['name' => $result->get('Name'),
                        'description' => $result->get('Description'),
                        'type' => $this->getType($result->get('HtmlType')),
                        'htmlType' => $result->get('HtmlType'),
                        'dbColumn' => $result->get('DbColumn'),
                        'show' => $result->get('Show')];
                }
                $dbObject = new DBObject($table, $objectId, $attrs, $this->app->getModule('DBManager'));
                return $dbObject;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function getEmptyHtml($dbObject) {
        $html = "";

        foreach ($dbObject->getAttrs() as $value) {
            if ($value['show']) {
                $template = $this->app->getModule('Templater')->
                        getTemplate($this->app->getSettings()->get('theme') .
                        "/DBObjectAttributes/" . $value['htmlType'] . ".html");

                $template->set('name', $value['name']);
                $template->set('description', $value['description']);

                $html .= $template->fetch();
            }
        }

        return $html;
    }

    public function getHtml($dbObject) {
        $html = "";

        $result = $dbObject->getResult();
        $result = $result->next();

        foreach ($dbObject->getAttrs() as $value) {
            if ($value['show']) {
                $template = $this->app->getModule('Templater')->
                        getTemplate($this->app->getSettings()->get('theme') .
                        "/DBObjectAttributes/" . $value['htmlType'] . ".html");

                $template->set('name', $value['name']);
                $template->set('description', $value['description']);
                $template->set('value', $result[$value['name']]);

                $html .= $template->fetch();
            }
        }

        return $html;
    }

    public function getType($name) {
        if (isset($this->types[$name])) {
            return $this->types[$name];
        } else {
            return NULL;
        }
    }

}
